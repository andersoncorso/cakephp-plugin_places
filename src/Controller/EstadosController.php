<?php
namespace Places\Controller;

use Places\Controller\AppController;

/**
 * Estados Controller
 *
 * @property \Places\Model\Table\EstadosTable $Estados
 *
 * @method \Places\Model\Entity\Estado[] paginate($object = null, array $settings = [])
 */
class EstadosController extends AppController
{

}
