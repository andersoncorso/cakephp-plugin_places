<?php
namespace Places\Controller;

use Places\Controller\AppController;

/**
 * Regioes Controller
 *
 * @property \Places\Model\Table\RegioesTable $Regioes
 *
 * @method \Places\Model\Entity\Regio[] paginate($object = null, array $settings = [])
 */
class RegioesController extends AppController
{

}
