<?php
namespace Places\Controller;

use Places\Controller\AppController;

/**
 * Municipios Controller
 *
 * @property \Places\Model\Table\MunicipiosTable $Municipios
 *
 * @method \Places\Model\Entity\Municipio[] paginate($object = null, array $settings = [])
 */
class MunicipiosController extends AppController
{

}
